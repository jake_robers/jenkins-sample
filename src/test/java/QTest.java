// QTest: test code for cube ("Q" = "cube, sort of)
//
// For consistency with Maven projects, this should be in the directory src/test/java
//

package cube;

import static org.junit.Assert.*;
import org.junit.Test;

public class QTest {

   @Test
   public void cubeSmallPositives() {
      assertEquals(1, Cubed.cube(1));
      assertEquals(125, Cubed.cube(5));
      assertEquals(729, Cubed.cube(9));
   }

   @Test
   public void cubeNegatives() {
      assertEquals(-1, Cubed.cube(-1));
      assertEquals(-64, Cubed.cube(-4));
   }

   @Test
   public void cubeZero() {
      assertEquals(0, Cubed.cube(0));
   }
} 

