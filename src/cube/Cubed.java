//
// Cubed.java: prompt user for a number and print its cube
//
// For consistency with Maven, this should be in the directory src/cube
//

package cube;

import java.util.Scanner;

// simple class to compute cubes with a main to prompt for a number and
//   print the result
public class Cubed { 

   // return x to the third power (computed efficiently - don't use pow for this)
   public static int cube(int x) {
      return x * x * x;
   }

   public static void main(String[] args) {
      Scanner in = new Scanner(System.in);
      System.out.print("Enter number: ");
      int x = in.nextInt();
      System.out.println("Cubed: " + cube(x));
   }
}
